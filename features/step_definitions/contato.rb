Dado('que acesso a pagina principal da {string}') do |liga_educacional|
    url = DATA[liga_educacional]
    visit(url) 
end
  
Quando('preencher os campos com dados válidos') do
    @contato.botaoMenu    

    # Para garantir que os dados serão setados apenas no primeiro formulário de contato.
    within('form#form-contato') do
        @contato.formulario
    end

    @contato.enviar
end
  
Então('devo ver uma mensagem de erro {string}') do |string|
    @contato.mensagem_erro_cadastro
end