#language:pt

Funcionalidade: Formulário de contato 

Cenário: Formulário com mensagem de erro  

Dado que acesso a pagina principal da "liga_educacional"
Quando preencher os campos com dados válidos 
Então devo ver uma mensagem de erro "Ooops! Faltou algum dado."