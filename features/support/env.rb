require 'capybara'
require 'capybara/cucumber'
require 'faker'
require 'rspec'
require 'selenium-webdriver'
require 'pry'
require 'rubocop-faker'



Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.default_max_wait_time = 30
   
end

EL = YAML.load_file(File.join(Dir.pwd, "./data/elements.yml"))
DATA = YAML.load_file(File.join(Dir.pwd, "./data/data.yml"))
