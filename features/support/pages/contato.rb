class Contato
    include Capybara::DSL
    include RSpec::Matchers

    def initialize

        @botao_menu               = EL['botao_menu']
        @link_contato             = EL['link_contato']
        @campo_nome               = EL['campo_nome']
        @campo_telefone           = EL['campo_telefone']
        @campo_email              = EL['campo_email']
        @campo_assunto            = EL['campo_assunto']
        @campo_instituição        = EL['campo_instituição']
        @campo_cargo              = EL['campo_cargo']
        @campo_mensagem           = EL['campo_mensagem']
        @botão_enviar             = EL['botão_enviar']
        @mensagem_erro            = EL['mensagem_erro']
    end

    def botaoMenu  
        find(@botao_menu).click
        find(@link_contato).click
    end

    def formulario
        # Rola a página até o formulário
        page.execute_script("$('html, body').animate({
            scrollTop: ($('#form-contato').offset().top)
        }, 400)")

        nome        =  Faker::Beer.name.titleize        
        nome_formatado = nome.gsub(" ","")
        find(@campo_nome).set nome

        telefone    =  "11951049444" 
        find(@campo_telefone).set telefone
        
        email        =  "#{nome_formatado.downcase}@teste.com.br"
        find(@campo_email).set email
        
        assunto     = Faker::Lorem.paragraph(1)
        find(@campo_assunto).set assunto    

        page.execute_script("$('html, body').animate({
            scrollTop: ($('#form-contato').offset().top + 300)
        }, 400)")
        
        instituicao = Faker::Company.name
        find(@campo_instituição).set instituicao
        
        find(@campo_cargo).click
    
        mensagem    = Faker::Lorem.paragraph(2)
        find(@campo_mensagem).set mensagem

    end
    
    def enviar
        find(@botão_enviar).click  
    end
    
    def mensagem_erro_cadastro

    

        find(@mensagem_erro, :visible => true)

        
        # if textoExiste == true
        #     puts 'Botão existe'
        # end
        
    end
        
end
